<?php
use Phinx\Migration\AbstractMigration;

class AddNewsLetterTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $newsTable = $this->table('news');
        $newsTable
            ->addColumn('name', 'string')
            ->addColumn('subject', 'string')
            ->addColumn('users_id', 'integer')
            ->addColumn('options', 'string',['limit'=>30000])
            ->addColumn('created', 'datetime')
            ->addColumn('template', 'string')
            ->create();


    }

    /**
     * Migrate Up.
     */
    public function up()
    {

    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}
