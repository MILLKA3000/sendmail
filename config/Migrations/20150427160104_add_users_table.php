<?php

use Phinx\Migration\AbstractMigration;

class AddUsersTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     */
    public function change()
    {
    $usersTable = $this->table('users');
    $usersTable
    ->addColumn('fname', 'string')
    ->addColumn('lname', 'string')
    ->addColumn('email', 'string')
    ->addColumn('password', 'string')
    ->addColumn('created', 'datetime')
    ->addColumn('modified', 'datetime')
    ->create();


    }

    
    /**
     * Migrate Up.
     */
    public function up()
    {
    
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}

