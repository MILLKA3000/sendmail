<?php
use Phinx\Migration\AbstractMigration;

class AddTopicsType extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('topics_types');
        $table
            ->addColumn('name', 'string')
            ->addColumn('style', 'string')
            ->addColumn('note', 'string')
            ->addColumn('created', 'datetime');
        $table->create();
    }
}
