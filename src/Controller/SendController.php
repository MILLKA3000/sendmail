<?php
namespace App\Controller;
use Cake\Network\Email\Email;


class SendController extends AppController
{

    private $options=[];


    public function init($id)
    {
        $this->loadModel('News');
        $this->options = $this->News->find('all',array('conditions'=>array('id'=>$id)))->first();

    }

    public function _previewEmail($id)
    {
        $this->init($id);
        $this->layout = 'ajax';
        $this->set(['options'=>json_decode($this->options->options)]);
        $this->render('/Email/html/'.$this->options->template);
    }

    public function index($id)
    {
        return $this->redirect(['controller'=>'Send','action' => '_previewEmail',$id]);
    }

    public function send_email($id)
    {
        $emailsArray = json_decode($this->request->data['emails']);

        $this->init($id);
        if (isset($this->options)) {
            foreach($emailsArray as $emails){
                $email = new Email();
                $email->transport('default');
                $email->template($this->options->template)
                    ->emailFormat('html')
                    ->subject($this->options->subject)
                    ->viewVars(['options' => json_decode($this->options->options)])
                    ->from('s.tchekanov@svitla.com');
                    $email->to($emails->text);
    //            ->to('ninaa@svitla.com')
                $send = $email->send();
            }
            if ($send){
                $this->Flash->success('Sending email');
            }else{
                $this->Flash->error('Error sending!');
            }

        }else{
            $this->Flash->error('Error sending! Not data!');
        }
        return $this->redirect(['controller'=>'News','action' => 'index']);
    }
}