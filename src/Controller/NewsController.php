<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * News Controller
 *
 * @property \App\Model\Table\NewsTable $News
 */
class NewsController extends AppController
{
    private $options=[];

    public function blocktopicks()
    {
        $this->layout = false;
    }

    public function imgadd()
    {
        $this->layout = 'ajax';
        $this->autoRender=false;
        $date = date_create();
        if ( !empty( $_FILES ) ) {
            $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
            $_FILES[ 'file' ][ 'name' ] = date_timestamp_get($date).'-'.$_FILES[ 'file' ][ 'name' ];
            $uploadPath =  WWW_ROOT."img".DIRECTORY_SEPARATOR.$_FILES[ 'file' ][ 'name' ];
            $uploadPath = str_replace(' ', '_', $uploadPath);
            move_uploaded_file( $tempPath, $uploadPath );
            $answer = array( 'answer' => 'File transfer completed','filename'=> str_replace(' ', '_', $_FILES[ 'file' ][ 'name' ]) );
            $json = json_encode( $answer );
            echo $json;
        } else {
            echo 'No files';
        }

    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $this->set('news', $this->paginate($this->News));
        $this->set('_serialize', ['news']);
    }

    /**
     * View method
     *
     * @param string|null $id News id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $news = $this->News->get($id, [
            'contain' => ['Users']
        ]);
        $this->set('news', $news);
        $this->set('_serialize', ['news']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

        $news = $this->News->newEntity();
        if ($this->request->is('post')) {
//            $this->request->data['options']=json_encode($this->options);
            $this->request->data['users_id']=$_SESSION['Auth']['User']['id'];
            $news = $this->News->patchEntity($news, $this->request->data);

            if ($this->News->save($news)) {
                $this->Flash->success('The news has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The news could not be saved. Please, try again.');
            }
        }
        $users = $this->News->Users->find('list', ['limit' => 200]);
        $this->set(compact('news', 'users'));
        $this->set('_serialize', ['news']);
        $this->render('edit');
    }

    /**
     * Edit method
     *
     * @param string|null $id News id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $news = $this->News->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
//            $this->request->data['options']=json_encode($this->options);
            $news = $this->News->patchEntity($news, $this->request->data);
            if ($this->News->save($news)) {
                $this->Flash->success('The news has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The news could not be saved. Please, try again.');
            }
        }
        $users = $this->News->Users->find('list', ['limit' => 200]);
        $this->set(compact('news', 'users'));
        $this->set('_serialize', ['news']);
    }

    /**
     * Delete method
     *
     * @param string|null $id News id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $news = $this->News->get($id);
        if ($this->News->delete($news)) {
            $this->Flash->success('The news has been deleted.');
        } else {
            $this->Flash->error('The news could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }
}
