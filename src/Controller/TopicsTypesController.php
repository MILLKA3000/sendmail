<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TopicsTypes Controller
 *
 * @property \App\Model\Table\TopicsTypesTable $TopicsTypes
 */
class TopicsTypesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('topicsTypes', $this->paginate($this->TopicsTypes));
        $this->set('_serialize', ['topicsTypes']);
    }

    /**
     * View method
     *
     * @param string|null $id Topics Type id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $topicsType = $this->TopicsTypes->get($id, [
            'contain' => []
        ]);
        $this->set('topicsType', $topicsType);
        $this->set('_serialize', ['topicsType']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $topicsType = $this->TopicsTypes->newEntity();
        if ($this->request->is('post')) {
            $topicsType = $this->TopicsTypes->patchEntity($topicsType, $this->request->data);
            if ($this->TopicsTypes->save($topicsType)) {
                $this->Flash->success('The topics type has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The topics type could not be saved. Please, try again.');
            }
        }
        $this->set(compact('topicsType'));
        $this->set('_serialize', ['topicsType']);
        $this->render('edit');
    }

    /**
     * Edit method
     *
     * @param string|null $id Topics Type id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $topicsType = $this->TopicsTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $topicsType = $this->TopicsTypes->patchEntity($topicsType, $this->request->data);
            if ($this->TopicsTypes->save($topicsType)) {
                $this->Flash->success('The topics type has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The topics type could not be saved. Please, try again.');
            }
        }
        $this->set(compact('topicsType'));
        $this->set('_serialize', ['topicsType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Topics Type id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $topicsType = $this->TopicsTypes->get($id);
        if ($this->TopicsTypes->delete($topicsType)) {
            $this->Flash->success('The topics type has been deleted.');
        } else {
            $this->Flash->error('The topics type could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }


    public function _gettypes()
    {
        $this->layout = 'ajax';
        $this->autoRender=false;
        $topicsType = $this->TopicsTypes->find('all')->toArray();
        echo json_encode($topicsType);

    }
}
