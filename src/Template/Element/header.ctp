<div class="navbar navbar-default " role="navigation">
    <div class="container-fluid hr-bottom" style="border-bottom-color: darkgrey!important;">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand left" href="#">Svitla NewsLetter</a>
        </div>

        <ul class="nav navbar-nav navbar-right" style="margin: 0;">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Menu <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <div class="clearfix visible-xs">
                        <?= $this->element('menu');?>
                    </div>
                    <li><a href='/users/view/<?= $_SESSION['Auth']['User']['id'] ?>'>My Profile</a></li>
                    <li class="divider"></li>
                    <li><a href='/users/logout'>logout</a></li>
                </ul>
            </li>
        </ul>

    </div>
</div>