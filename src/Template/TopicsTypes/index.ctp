<div class="col-lg-10 col-md-10 col-xs-12 col-sm-9 hr-bottom">
    <ul class="nav-pills  navbar-right" style="list-style: none;">
        <li><?= $this->Html->link(__('New Topics Type'), ['action' => 'add'],['class'=>'btn btn-info']) ?></li>
    </ul>
</div>
<div class="col-lg-10 col-md-10 col-xs-12 col-sm-9">
    <table cellpadding="0" cellspacing="0" class="table text-left">
    <thead>
        <tr>
            <th width="50%"><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($topicsTypes as $topicsType): ?>
        <tr>
            <td>
                <div style="<?= h($topicsType->style) ?>;text-align: center;"><?= h($topicsType->name) ?></div>
            </td>

            <td><?= h($topicsType->created) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $topicsType->id], ['class'=>'btn btn-warning']) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $topicsType->id], ['class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $topicsType->name)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
