<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Topics Type'), ['action' => 'edit', $topicsType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Topics Type'), ['action' => 'delete', $topicsType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $topicsType->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Topics Types'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Topics Type'), ['action' => 'add']) ?> </li>
    </ul>
</div>
<div class="topicsTypes view large-10 medium-9 columns">
    <h2><?= h($topicsType->name) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Name') ?></h6>
            <p><?= h($topicsType->name) ?></p>
            <h6 class="subheader"><?= __('Style') ?></h6>
            <p><?= h($topicsType->style) ?></p>
            <h6 class="subheader"><?= __('Note') ?></h6>
            <p><?= h($topicsType->note) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($topicsType->id) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Created') ?></h6>
            <p><?= h($topicsType->created) ?></p>
        </div>
    </div>
</div>
