<div class="col-lg-10 col-md-10 col-xs-12 col-sm-9 hr-bottom">
    <ul class="nav-pills  navbar-right" style="list-style: none;">
        <li><?= $this->Html->link(__('List Topics Types'), ['action' => 'index'],['class'=>'btn btn-info']) ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $topicsType->id],
                ['class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $topicsType->name)]
            )
            ?></li>

    </ul>
</div>
<div ng-app="typetopics" ng-controller="typetopicsCtrl">
    <div class="col-lg-5 col-md-5 col-xs-12 col-sm-10">
        <?= $this->Form->create($topicsType); ?>
        <fieldset>
            <legend><?= __('Name') ?></legend>
            <?php echo $this->Form->input('name',['class'=>'form-control','ng-model'=>'data.name','label'=>'']); ?>
            <legend><?= __('Style') ?></legend>
            <?php echo $this->Form->input('style',['class'=>'form-control','ng-model'=>'data.style','label'=>'']); ?>
        </fieldset>
        <?= $this->Form->button(__('Save'),['class'=>'btn btn-success','style'=>'margin-top:25px;']) ?>
        <?= $this->Form->end() ?>
    </div>

    <div class="col-lg-5 col-md-5 col-xs-12 col-sm-10" style="padding-top: 50px">
        <div style="{{data.style}};text-align: center;">{{data.name}}
        </div>
    </div>
</div>
<script type="application/javascript">
    var module = angular.module('typetopics',[]);
    module.controller("typetopicsCtrl", function($scope,$interval) {

        $scope.data={"style":'<?= $topicsType->style ?>',"name":'<?= $topicsType->name ?>'};

    });
</script>