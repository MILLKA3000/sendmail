<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Topics Types'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="topicsTypes form large-10 medium-9 columns">
    <?= $this->Form->create($topicsType); ?>
    <fieldset>
        <legend><?= __('Add Topics Type') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('style');
            echo $this->Form->input('note');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Save')) ?>
    <?= $this->Form->end() ?>
</div>
