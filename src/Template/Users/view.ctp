<div class="col-lg-10 col-md-10 col-xs-12 col-sm-9 hr-bottom">
    <ul class="nav-pills  navbar-right" style="list-style: none;">
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index'],['class'=>'btn btn-info']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add'],['class'=>'btn btn-info']) ?> </li>
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id],['class'=>'btn btn-warning']) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>

    </ul>
</div>
<div class="col-lg-10 col-md-10 col-xs-12 col-sm-9">
    <div class="row text-left">
        <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6 alert alert-info">
            <h6 class="subheader"><?= __('Fname') ?></h6>
            <p><?= h($user->fname) ?></p>
            <h6 class="subheader"><?= __('Lname') ?></h6>
            <p><?= h($user->lname) ?></p>
            <h6 class="subheader"><?= __('Email') ?></h6>
            <p><?= h($user->email) ?></p>
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($user->id) ?></p>
        </div>
        <div class="col-lg-5 col-md-5 col-xs-5 col-sm-5 alert-info">
            <h6 class="subheader"><?= __('Created') ?></h6>
            <p><?= h($user->created) ?></p>
            <h6 class="subheader"><?= __('Modified') ?></h6>
            <p><?= h($user->modified) ?></p>
        </div>
    </div>
</div>
