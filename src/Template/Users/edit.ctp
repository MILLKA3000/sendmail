<div class="col-lg-10 col-md-10 col-xs-12 col-sm-9 hr-bottom">
    <ul class="nav-pills  navbar-right" style="list-style: none;">
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index'],['class'=>'btn btn-info']) ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $user->id],
                ['class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $user->id)]
            )
        ?></li>

    </ul>
</div>
<div class="col-lg-5 col-md-5 col-xs-12 col-sm-10">
    <?= $this->Form->create($user); ?>
    <fieldset>
        <?php
            echo $this->Form->input('fname',['class'=>'form-control']);
            echo $this->Form->input('lname',['class'=>'form-control']);
            echo $this->Form->input('email',['class'=>'form-control']);
            echo $this->Form->input('password',['class'=>'form-control']);
        ?>
    </fieldset>
    <br>
    <?= $this->Form->button(__('save'),['class'=>'btn btn-success']) ?>
    <?= $this->Form->end() ?>
</div>
