
<?= $this->Flash->render('auth') ?>
<?= $this->Form->create() ?>
    <fieldset>
        <legend style="max-width:500px;"><?= __('Please enter your username and password') ?></legend>
        <div class="center-block" style="max-width:300px;">
            <?= $this->Form->input('email',['class'=>'form-control','placeholder'=>'Email address']) ?>
            <?= $this->Form->input('password',['class'=>'form-control','placeholder'=>'Password']) ?>
        </div>
    </fieldset>
<div class="center-block" style="max-width:300px;"><br>
<?= $this->Form->button(__('Sign in'),['class'=>'btn btn-lg btn-primary btn-block']); ?>
    <a  href="/users/oauth2callback" class="btn btn-lg btn-danger btn-block btn-social btn-google-plus">
        <i class="fa fa-google"></i> Sign in with Google
    </a>
</div>
<?= $this->Form->end() ?>

