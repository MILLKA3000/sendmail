<div class="col-lg-10 col-md-10 col-xs-12 col-sm-9 hr-bottom">
    <ul class="nav-pills  navbar-right" style="list-style: none;">
        <li><?= $this->Html->link(__('New NewsLetter'), ['action' => 'add'],['class'=>'btn btn-info']) ?></li>
    </ul>
</div>
<div class="col-lg-10 col-md-10 col-xs-12 col-sm-9">
    <table cellpadding="0" cellspacing="0" class="table text-left">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('name') ?></th>
            <th><?= $this->Paginator->sort('subject') ?></th>
            <th style="width:20%" ><?= $this->Paginator->sort('users_id') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th><?= $this->Paginator->sort('template') ?></th>
            <th style="width:30%" class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($news as $news): ?>
        <tr>
            <td><?= h($news->name) ?></td>
            <td><?= h($news->subject) ?></td>
            <td>
                <?= $news->has('user') ? $this->Html->link($news->user->email, ['controller' => 'Users', 'action' => 'view', $news->user->id]) : '' ?>
            </td>
            <td><?= h($news->created) ?></td>
            <td><?= h($news->template) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('Preview'), ['controller'=>'Send','action' => '_previewEmail', $news->id], ['class'=>'btn btn-success','target' => '_blank']) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $news->id],['class'=>'btn btn-warning']) ?>
                <button type="button" class="buttonsend btn btn-success" data-toggle="modal" id_email="<?= $news->id ?>" data-target="#send">
                    Send
                </button>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="send" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Sending mail</h4>
            </div>
            <?= $this->Form->create(null, [
                'class' => 'formsend']); ?>
            <div class="modal-body text-left">
                <?= $this->Form->create($news); ?>
                <div ng-app="plunker" ng-controller="MainCtrl">
                    <input type="text" name="emails" id="hideemails" style="display:none;">
                    <div class="radio">
                        <label>
                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="my" checked ng-click="email.send('my')">
                            Only For Me
                        </label>
                        <input type="text" class="form-control" name="myEmail" placeholder="Text input" disabled="disabled" value="<?= $_SESSION['Auth']['User']['email']?>">
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="optionsRadios" id="optionsRadios2" value="other" ng-click="email.send('other')">
                            Add Other Emails
                        </label>

                            <tags-input class="emails"
                                        ng-model="email"
                                        placeholder="example@domain"
                                        replace-spaces-with-dashes="false">
                            </tags-input>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <?= $this->Form->button(__('Send'),['class'=>'btn btn-success','id'=>'sending_email']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<script type="application/javascript">
    var app = angular.module('plunker', ['ngTagsInput']);

    app.controller('MainCtrl', function($scope, $http,$interval) {
        $scope.email = [{'text':'<?= $_SESSION['Auth']['User']['email']?>'}];

        $scope.email.send = function(type) {
            if(type=='my'){
                $scope.email.splice(0, 100);
                $scope.email.push({'text':'<?= $_SESSION['Auth']['User']['email']?>'});
            }
            console.log($scope.email);
        };

        $interval( function(){ $('#hideemails').val(JSON.stringify($scope.email));}, 500);

    });

    $('.buttonsend').on('click',function(){
        $('.formsend').attr('action','/Send/send_email/'+$(this).attr('id_email'));
    })
</script>