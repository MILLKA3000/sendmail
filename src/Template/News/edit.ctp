<div class="col-lg-10 col-md-10 col-xs-12 col-sm-9 hr-bottom">
    <ul class="nav-pills  navbar-right" style="list-style: none;">
        <li><?= $this->Html->link(__('List NewsLetter'), ['action' => 'index'],['class'=>'btn btn-info']) ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $news->id],
                ['class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $news->id)]
            )
            ?></li>

    </ul>
</div>
<div class="col-lg-10 col-md-10 col-xs-12 col-sm-9 text-left">
    <?= $this->Form->create($news); ?>
    <fieldset>
        <legend><?= __('Edit NewsLetter') ?></legend>
        <?php
            echo $this->Form->input('name',['class'=>'form-control']);
            echo $this->Form->input('subject',['class'=>'form-control']);
            echo $this->Form->input('options',['id'=>'options','style'=>'display:none','label'=>'']);
            echo $this->Form->input('columns',['id'=>'columns','style'=>'display:none','label'=>'']);
        ?>
        <div ng-app="newsletter">
            <div ng-controller="NewsCtrl">

                <fieldset style="margin-top: 25px;">
                    <legend>Body Email</legend>
                    <div class="generalSetting row" style="margin: 25px 0;">
                        <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                            <?php echo $this->Form->input('template', ['class'=>'form-control',
                                'ng-change'=>'data.changeTemplate()',
                                'ng-model'=>'data.template',
                                'options' => [
                                    ['value'=>'newsletter2columns','text'=>'NewsLetter(Static)(2columns)'],
                                    ['value'=>'newsletter_resp2columns','text'=>'NewsLetter(Responsive)(2columns)'],
                                    ['value'=>'newsletter1columns','text'=>'NewsLetter(Static)(1columns)']

                            ]]);?>
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-12-sm-12">
                            <label for="Nlogo">Link for logo</label>
                            <input type="text" ng-model="data.logo" id="Nlogo" ng-click="data.logoinp($event)" class="form-control" placeholder="Enter a name here">
                        </div>

                        <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                            <label for="Nbody">Style body</label>
                            <input type="text" ng-model="data.style_body" id="Nbody" class="form-control" placeholder="Style">
                        </div>

                        <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                            <label for="Nbody_img">Style body image</label>
                            <input type="text" ng-model="data.style_body_img" id="Nbody_img" ng-click="data.body_img($event)" class="form-control" placeholder="Url">
                        </div>

                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12" style="{{data.style_body}};background-image: url({{data.style_body_img}});background-repeat: repeat-y;background-size: cover;">
                        <img src="{{data.logo}}" style="width: 100%">
                        <div class="leftNewsLetter col-lg-6 col-md-6 col-xs-6 col-sm-6" style="margin: 10px 0;">
                            <legend>Add new block in left side</legend>
                            <div ng-repeat="(n, value) in block">
                                <span style="font-size: 10pt!important;margin: 0 5px 20px 0;{{value.style}}" class="btn pull-left" ng-click="block.addBlock(n,data.left)">{{value.name}}</span>
                            </div>
                            <legend></legend>
                        </div>

                        <div class="rightNewsLetter col-lg-6 col-md-6 col-xs-6 col-sm-6"  style="margin: 10px 0;">
                            <legend>Add new block in right side</legend>
                            <div ng-repeat="(n, value) in block">
                                <span style="font-size: 10pt!important;margin: 0 5px 20px 0;{{value.style}}" class="btn  pull-left" ng-click="block.addBlock(n,data.right)">{{value.name}}</span>
                            </div>
                            <legend></legend>
                        </div>


                        <div class="leftNewsLetter col-lg-6 col-md-6 col-xs-6 col-sm-6 ">
                            <block data='data' orient="data.left" side="left"></block>
                        </div>
                        <div class="rightNewsLetter col-lg-6 col-md-6 col-xs-6 col-sm-6 ">
                            <block data="data" orient='data.right' side="right"></block>

                        </div>
                    </div>
                </fieldset>
                <!-- Modal -->
                <div class="modal fade" id="imagedialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content" id="ng-app" ng-app="app">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Add image</h4>
                            </div>
                            <div class="row col-lg-12 col-md-12 col-xs-12 col-sm-12" style="margin: 0 auto;">
                                <div style="margin:20px 0;">
                                    <input class="center-block text-center" type="file" nv-file-select="" uploader="uploader"/>
                                </div>

                                <ul>
                                    <li ng-repeat="item in uploader.queue">
                                        Name: <span ng-bind="item.file.name"></span><br/>
                                        <a type="button" class="btn btn-success btn-xs" ng-click="item.upload()" ng-disabled="item.isReady || item.isUploading || item.isSuccess">Upload</a>
                                    </li>
                                </ul>
                                <div>
                                    <img src="{{preview}}" id="preview" class="hidden" alt="Preview image" style="margin:20px auto;max-width: 100%;">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="text" class="form-control" style="width:80%;float:left;" name="imgInput" ng-model="currentNode.href" id="imgInput" placeholder="Input image" value="{{currentNode.href}}">
                                <button type="button" id="okImg" class="btn btn-primary" data-dismiss="modal">Ok</button>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </fieldset>
    <?= $this->Form->button(__('Save'),['class'=>'btn btn-success center-block','style'=>'margin-top:25px;']) ?>
    <?= $this->Form->end() ?>
</div>

<script type="application/javascript">

    if("<?= $news->template; ?>".search("(1columns)") != -1){
        $('.rightNewsLetter').hide();
        $('#columns').val('940');
        $('.leftNewsLetter').attr('class','leftNewsLetter col-lg-12 col-md-12 col-xs-12 col-sm-12');
    }else if("<?= $news->template; ?>".search("(2columns)") != -1){
        $('#columns').val('430');
    }


    var module = angular.module('newsletter', ['textAngular','angularFileUpload']);

    module.controller("NewsCtrl", function($scope,gettopics,$interval,FileUploader) {

        var uploader = $scope.uploader = new FileUploader({
            url: '/News/imgadd/'
        });

        $scope.preview='';

        uploader.filters.push({
            name: 'imageFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        });
        uploader.onErrorItem = function(fileItem, response, status, headers) {
            $('#preview').attr('class','hidden');
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            var linkImage = 'http://<?= $_SERVER['SERVER_NAME']; ?>'+'/img/'+response.filename;
            $('#preview').attr('class','show');
            $('#imagedialog #imgInput').val(linkImage);
            $scope.preview=linkImage;
            uploader.queue.splice(0, 1);
        };

        $scope.data={'left':[],'right':[]};
        $scope.left="Left",
        $scope.right="Right";
        <?php if ($news->options!=''): ?>
            $scope.data =  <?= $news->options; ?>;
        <?php endif;?>

        $scope.data.logoinp = function() {
            $('#imagedialog').modal('show');
            $('#imagedialog').on('hide.bs.modal', function (e) {
                $scope.data.logo = $('#imagedialog #imgInput').val();
                $(e.currentTarget).unbind();
            });
        };

        $scope.data.body_img = function() {
            $('#imagedialog').modal('show');
            $('#imagedialog').on('hide.bs.modal', function (e) {
                $scope.data.style_body_img = $('#imagedialog #imgInput').val();
                $(e.currentTarget).unbind();
            });
        };

        $scope.data.template="<?= $news->template; ?>";
        $scope.block = [];
        $scope.data.changeTemplate = function() {
            $scope.dyamicTemplate(this);
        };
        $scope.dyamicTemplate = function(ar) {
            if(ar.template.search("(1columns)") != -1){
                $('.rightNewsLetter').hide();
                $('.leftNewsLetter').attr('class','leftNewsLetter col-lg-12 col-md-12 col-xs-12 col-sm-12');
                $('#columns').val('940');
                angular.forEach($scope.data.right, function (k, v) {
                    $scope.data.left.push(k);
                });
                $scope.data.right.splice(0, 100);

            }else{
                $('.leftNewsLetter').attr('class','leftNewsLetter col-lg-6 col-md-6 col-xs-6 col-sm-6');
                $('#columns').val('430');
                $('.rightNewsLetter').show();
            }
        }
        /*
        * get topicks
        */

        gettopics.success(function(data) {
            angular.forEach(data, function (k, v) {
                data[v].topic=[];
            });

            $scope.block = data;

            $scope.block.addBlock = function(num,block) {
                u = angular.copy($scope.block[num]);
                block.push(u);
                console.log(block);
            };

        });
        $scope.callAtInterval = function() {
            $('#options').text(JSON.stringify($scope.data));
        }
        $interval( function(){ $scope.callAtInterval();}, 500);

    });

    module.directive('block', function() {
        return {
            restrict: 'E',
            scope: {
                data: '=',
                orient: '=',
                side:'='
            },
            templateUrl: '/News/blocktopicks/',
            link:function(scope, element, attrs) {
                scope.data.add = function(num,block) {
                    block[num].topic.push({text:'',title:''});
                };

                scope.data.toBlock = function(num,value,side) {
                    if(side == "Right"){
                        scope.data.left.push(value);
                        scope.data.right.splice(num, 1);
                    }
                    if(side == "Left"){
                        scope.data.right.push(value);
                        scope.data.left.splice(num, 1);
                    }
                };

                scope.data.BlockDelete = function(num,block) {
                    block.splice(num, 1);
                };

                scope.data.delItem = function(topik,num,block) {
                    block[topik].topic.splice(num, 1);
                };
            }
        };


    });

    /*
    service for get topics
    */

    module.factory('gettopics', ['$http', function($http) {
        return $http.get('/TopicsTypes/_gettypes')
            .success(function(data) {
                return data;
            })
            .error(function(err) {
                return err;
            });
    }]);

    /*
    Config text angular
     */

    module.config(['$provide',
        function($provide) {
            $provide.decorator('taOptions', ['taRegisterTool', '$delegate',
                function(taRegisterTool, taOptions) {
                    taOptions.toolbar = [['p'],['h1','h3','h5'],['clear'],['html', 'insertLink', 'customInsertImage'],['justifyLeft', 'justifyCenter', 'justifyRight']];
                    taRegisterTool('customInsertImage', {
                        iconclass: "fa fa-picture-o",
                        action: function($deferred) {
                            var self = this;
                            var savedSelection = rangy.saveSelection();
                            $('#imagedialog #imgInput').val('');
                            $('#imagedialog').modal('show');
                            $('#imagedialog').on('hide.bs.modal', function (e) {
                                rangy.restoreSelection(savedSelection);
                                im = "<img src='"+$('#imagedialog #imgInput').val()+"' width='"+$('#columns').val()+"' align='center' style='width:100%;max-width:"+$('#columns').val()+";  margin-left: auto; margin-right: auto;'/>";
                                self.$editor().wrapSelection('insertHTML', im);
                                $deferred.resolve();
                            })
                            return false;
                        },
                    });
                    return taOptions;
                }
            ]);
        }
    ]);
</script>


