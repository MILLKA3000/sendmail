<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit NewsLetter'), ['action' => 'edit', $news->id]) ?> </li>
        <li><?= $this->Html->link(__('List NewsLetter'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New NewsLetter'), ['action' => 'add']) ?> </li>
        <li><?= $this->Form->postLink(__('Delete NewsLetter'), ['action' => 'delete', $news->id], ['confirm' => __('Are you sure you want to delete # {0}?', $news->id)]) ?> </li>
    </ul>
</div>
<div class="news view large-10 medium-9 columns">
    <h2><?= h($news->name) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Name') ?></h6>
            <p><?= h($news->name) ?></p>
            <h6 class="subheader"><?= __('Subject') ?></h6>
            <p><?= h($news->subject) ?></p>
            <h6 class="subheader"><?= __('User') ?></h6>
            <p><?= $news->has('user') ? $this->Html->link($news->user->email, ['controller' => 'Users', 'action' => 'view', $news->user->id]) : '' ?></p>
            <h6 class="subheader"><?= __('Template') ?></h6>
            <p><?= h($news->template) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($news->id) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Created') ?></h6>
            <p><?= h($news->created) ?></p>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Options') ?></h6>
            <?= $this->Text->autoParagraph(h($news->options)); ?>

        </div>
    </div>
</div>
