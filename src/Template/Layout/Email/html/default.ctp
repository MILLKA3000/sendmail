<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
    <link href='http://fonts.googleapis.com/css?family=Helvetica+Neue:400,500,700' rel='stylesheet' type='text/css'>
    <title><?= $this->fetch('title') ?></title>
    <style>
        @media only screen and (min-width: 200px) and (max-width: 767px) {
            #bod{
                background-image:none!important;
            }
        }
    </style>
</head>
<body>
        <?= $this->fetch('content') ?>
</body>
</html>
