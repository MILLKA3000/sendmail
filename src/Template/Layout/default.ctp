<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
<!--    <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
<!--    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>-->
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://mbenford.github.io/ngTagsInput/css/ng-tags-input.min.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <?= $this->Html->css('custom.css') ?>
    <?= $this->Html->css('bootstrap.css') ?>
    <?= $this->Html->script('jquery-1.11.2.min.js') ?>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.5/angular.min.js"></script>
    <?= $this->Html->script('bootstrap.js') ?>
    <?= $this->Html->script('textAngular.js') ?>
    <?= $this->Html->script('angular-ui-bootstrap.js') ?>
    <?= $this->Html->script('angular-file-upload.js') ?>
    <script src="http://mbenford.github.io/ngTagsInput/js/ng-tags-input.min.js"></script>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>

    <header>
        <?php if(isset($_SESSION['Auth']['User']['id'])):?>
            <?= $this->element('header');?>
        <?php endif;?>
    </header>
    <div class="container-fluid">
        <div class="starter-template">
            <?= $this->Flash->render() ?>
            <div class="row">
                <?php if(isset($_SESSION['Auth']['User']['id'])):?>
                    <div class="hidden-xs">
                        <?= $this->element('menu');?>
                    </div>
                <?php endif;?>
                <?= $this->fetch('content') ?>
            </div>
        </div>
        <footer>
        </footer>
    </div>
</body>
</html>
