<table >
    <tr>
        <td style="background-image: url(<?= (isset((isset($options->style_body))?$options->style_body:'';_img))?(isset($options->style_body))?$options->style_body:'';_img:'';?>);background-repeat: repeat-y;">
            <!--[if !mso]><!--><table align="center"  cellpadding="0" cellspacing="0"  style="width:100%;"><!--<![endif]-->
            <!--[if gte mso 9]><table align="center" cellpadding="0" cellspacing="0" width="960" ><![endif]-->
            <tr style="<?= (isset($options->style_body))?$options->style_body:'';;?>">
                <td width="100%">
                    <img alt="header" id="img" width="1030" style='display:block;width:100%;' src="<?= $options->logo; ?>">
                </td>
            </tr>
            <tr style="<?= (isset($options->style_body))?$options->style_body:'';;?>">
                <td height="20" align="center">

                    <table width="96%" cellspacing="0" cellpadding="0" border="0" style="">
                        <tbody>
                            <tr style="<?= (isset($options->style_body))?$options->style_body:'';;?>"><td height="10"></td></tr>
                            <tr>

                                <td width="55%" valign="top">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0" >

                                        <tbody>
                                        <tr>
                                            <td>
                                        <?php foreach($options->left as $topic):?>
                                            <tr>
                                                <td style="<?= $topic->style?>" align="center"><?= $topic->name?></td>
                                            </tr>
                                            <?php $i=1; ?>
                                        <?php foreach($topic->topic as $item):?>
                                            <?php $item->img_outlook = "100%"; ?>
                                            <?php $item->orient = 'left'; ?>
                                                <?php $item->separator = ((count($topic->topic)==1)||($i==count($topic->topic)))? false : true ; ?>
                                                <?php echo $this->element('email/block', array('item' => $item))  ?>
                                                <?php $i++; ?>
                                        <?php  endforeach;?>
                                            <tr style="<?= (isset($options->style_body))?$options->style_body:'';;?>">
                                                <td height="15"></td>
                                            </tr>
                                        <?php endforeach;?>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td width="2%">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td width="45%" valign="top">
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <?php foreach($options->right as $topic):?>
                                                <tr>
                                                    <td style="<?= $topic->style?>" align="center"><?= $topic->name?></td>
                                                </tr>
                                                <?php $i=1; ?>
                                                <?php foreach($topic->topic as $item):?>
                                                    <?php $item->img_outlook = "100%"; ?>
                                                    <?php $item->orient = 'right'; ?>
                                                        <?php $item->separator = ((count($topic->topic)==1)||($i==count($topic->topic)))? false : true ; ?>
                                                        <?php echo $this->element('email/block', array('item' => $item))  ?>
                                                        <?php $i++; ?>
                                                <?php  endforeach;?>
                                                <tr style="<?= (isset($options->style_body))?$options->style_body:'';;?>">
                                                    <td height="15"></td>
                                                </tr>
                                                <?php endforeach;?>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
            <!--[if !mso]><!--><table align="center"  cellpadding="0" cellspacing="0"  style="width:100%;"><!--<![endif]-->
<!--[if gte mso 9]><table align="center" cellpadding="0" cellspacing="0" width="960" ><![endif]-->
        <tr style="<?= (isset($options->style_body))?$options->style_body:'';;?>">
            <td height="15">

            </td>
        </tr>
        <tr style="<?= (isset($options->style_body))?$options->style_body:'';;?>">
            <td width="100%" align="center" style="text-align:center;">
                <span class="MsoNormal" align="center" style="text-align:center;line-height:115%"><a href="https://twitter.com/svitlasystemsin" target="_blank"><span style="color:windowtext;text-decoration:none"><img border="0" width="33" height="33" src="http://svitla.com/mailimg/img/social/Twit.png" alt="twitter" class="CToWUd"></span></a><span lang="EN-US"><u></u><u></u></span></span>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span class="MsoNormal" align="center" style="text-align:center;line-height:115%"><a href="https://www.facebook.com/SvitlaSystems?fref=ts" target="_blank"><span style="color:windowtext;text-decoration:none"><img border="0" width="33" height="33" src="http://svitla.com/mailimg/img/social/f.png" alt="facebook" class="CToWUd"></span></a><span lang="EN-US"><u></u><u></u></span></span>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span class="MsoNormal" align="center" style="text-align:center;line-height:115%"><a href="https://www.linkedin.com/company/svitla-systems-inc" target="_blank"><span style="color:windowtext;text-decoration:none"><img border="0" width="33" height="33" src="http://svitla.com/mailimg/img/social/in.png" alt="linkedin" class="CToWUd"></span></a><span lang="EN-US"><u></u><u></u></span></span>
            </td>
        </tr>
        <tr style="<?= (isset($options->style_body))?$options->style_body:'';;?>">
            <td width="100%" align="center" style="text-align:center;">
            <span lang="EN-US" style="font-size:8.0pt;line-height:115%;font-family: Arial, Helvetica, sans-serif;color:#fe571d">
                Слідкуйте за новинами
            </span>
            </td>
        </tr>
        <tr style="<?= (isset($options->style_body))?$options->style_body:'';;?>"><td height="15">&nbsp;</td></tr>
</table>
        </td>
    </tr>
</table>