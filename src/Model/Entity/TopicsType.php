<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TopicsType Entity.
 */
class TopicsType extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'style' => true,
        'note' => true,
    ];
}
