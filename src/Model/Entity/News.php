<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * News Entity.
 */
class News extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'subject' => true,
        'users_id' => true,
        'options' => true,
        'template' => true,
        'user' => true,
    ];
}
